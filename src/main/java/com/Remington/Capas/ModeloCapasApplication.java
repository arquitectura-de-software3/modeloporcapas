package com.Remington.Capas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;


@SpringBootApplication



public class ModeloCapasApplication {

	public static void main(String[] args) {
		SpringApplication.run(ModeloCapasApplication.class, args);
	}

}
