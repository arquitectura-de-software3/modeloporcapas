package com.Remington.Capas.repositorio;

import com.Remington.Capas.modelo.Usuarios;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Usuarios, Integer> {
}
